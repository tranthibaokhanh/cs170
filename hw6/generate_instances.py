import random
import sys

try:
    input = raw_input
except NameError:
    pass

def main():
    print('At the prompt, enter a login and, optionally, a file to write the instances to. Enter each login in the format cs170-xx followed by any number of spaces and then the file name. When you are done entering logins, enter . to end the program.')
    while True:
        if not generate():
            break

def generate():
    login = input('Enter the next login or . if done: ').strip().split()
    if login[0] is '.':
        return False
    set_seed(to_int(login[0]))
    instances = ''
    for i in range(8):
        length = next_int(2,12)
        instance = ''
        moves = set()
        ratios = set()
        for j in range(length):
            move = next_int(2,200)
            cost = next_int(1,20)
            while move in moves or ((1.0 * move) / cost) in ratios:
                move = next_int(2, 200)
                cost = next_int(1, 20)
            moves.add(move)
            ratios.add((1.0 * move) / cost)
            instance += '({0:d}, {1:d}) '.format(move, cost)
        instances += instance + '\n'
    move = next_int(130, 165)
    instances += '({0:d}, 2) ({1:d}, 1)\n'.format(move + 1, move)
    move = next_int(165, 200)
    instances += '({0:d}, 2) ({1:d}, 1)\n'.format(move + 1, move)
    if (len(login) > 1):
        f = open(login[1], 'w')
        f.write(instances)
    else:
        print(instances)
    return True

def to_int(s):
    n = 0
    for c in s:
        n *= 77
        n += ord(c)
    return n

def set_seed(y):
    global x
    x = y

def next_int(i, j):
    global x
    x = (a*x + c) % m
    return i + abs(x % (j - i))

x = 0
m = 2**32
a = 1664525
c = 1013904223



if __name__ == '__main__':
	main()

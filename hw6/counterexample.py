import sys
import random
import util

try:
    input = raw_input
except NameError:
    pass

def main():
    # print('At the prompt, enter a login and, optionally, a file to write the instances to. Enter each login in the format cs170-xx followed by any number of spaces and then the file name. When you are done entering logins, enter . to end the program.')
    # find_n()
    moves = [
    [(32, 16),(186, 4),(76, 14),(96, 14),(158, 12),(106, 5),(160, 12)],
    [(121, 5),(185, 17),(131, 4),(51, 5)],
    [(132, 14),(186, 12),(6, 3),(14, 13),(2, 6),(78, 4),(88, 6)], [(7, 10),(115, 7),(151, 15),(113, 10)],
    [(76, 17),(64, 18),(196, 5),(124, 5),(60, 19),(32, 15),(54, 8),(102, 15),(136, 18)],
    [(133, 4),(27, 19),(123, 11),(139, 13),(35, 3),(39, 7)],
    [(108, 11),(30, 12),(162, 6),(196, 8),(158, 17),(72, 11),(48, 4)],
    [(85, 12),(117, 19),(63, 4),(75, 10),(37, 5),(67, 11),(155, 7),(21, 18),(129, 16),(81, 13)],
    [(136, 2),(135, 1)], [(166, 2),(165, 1)]]
    for move in moves:
    	i = 1
        costArray = [0,1]
    	while greedy_cost(move, i) == costArray[i]:
    		# print i
    		i += 1

    		if i > len(costArray) - 1:
    			# print "yes"

    			minCost = costArray[i - 1] + 1
    			toMin = set()
    			toMin.add(minCost)
    			for x, y in move:
    				if i % x == 0 and i >= x:
    					newIndex = int(i / x)
    					toMin.add(costArray[newIndex] + y)
    			minCost = min(toMin)
    			costArray.extend([minCost])
    		# print costArray

    	print "ERROR! on i = " + str(i)
    	print "greedy_cost = " + str(greedy_cost(move, i))
    	print "dp_cost = " + str(dp_cost(move, i))
    return
def greedy_cost(moves, n):
	# moves is an array of tuples
	cost = 0
	while n > 0:
		maxRatio = -1*float("inf")
		chosenMove = (-1, 1)
		for x, y in moves:
			if (x/y > maxRatio) and (n % x == 0) and (n >= x):
				maxRatio = x/y
				chosenMove = (x, y)
		if chosenMove[0] == -1:
			n = n - 1
			cost += 1
		else:
			n = n/chosenMove[0]
			cost += chosenMove[1]
	# print cost
	return cost

def dp_cost(moves, n):
	# initialize an empty cost array. Assume costArray[0] represents the cost of going from 1 to 0. 
	# n-1 is the min cost of going from n to n-1
	n = n + 1
	cost = 0
	costArray = [0] * n
	costArray[1] = 1
	i = 2
	while i < n:
		# check whether that value has been updated
		if costArray[i] == 0:
			minCost = costArray[i - 1] + 1
			toMin = set()
			toMin.add(minCost)

			for x, y in moves:
				if i % x == 0 and i >= x:
					newIndex = int(i / x)
					toMin.add(costArray[newIndex] + y)
			minCost = min(toMin)
			costArray[i] = minCost
		i += 1
	# print costArray
	# print costArray[n - 1]
	return costArray[n - 1]

if __name__ == "__main__":
    main()


	
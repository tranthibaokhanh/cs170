#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "lp_lib.h"


int
main (void) {
	lprec *lp;

  	lp = make_lp(0,4);

	FILE *fp;
	char buff[10000]; // buffer for reading in lines
	int child_nodes[500] = {0}; // 1 if node i is a child, 0 otherwise
	int num_nodes; // number of nodes in the instance
	int adj_matrix[500*500]; //adjacency matrix of 1's and 0's

	fp = fopen("test.in", "r");

	// reads the first line of the input, getting the number of nodes in the instance
	fgets(buff, sizeof buff, fp);
	num_nodes = atoi(buff);
	

	// reads the second line, getting the nodes that are children
	fgets(buff, sizeof buff, fp);
	char *p = buff;

	while (*p) {
		if (isdigit(*p)) {
			long val = strtol(p, &p, 10);
			val = val - 1;
			child_nodes[val] = 1;
		} else {
			p++;
		}
	}

	// reads in the rest of the instance, forming the adjacency matrix
	int row = 0;
	int col = 0;
	while (fgets(buff, sizeof buff, fp) != NULL) {
		col = 0;
		p = buff;
		while (*p) {
			if (isdigit(*p)) {
				adj_matrix[row*500 + col] = strtol(p, &p, 10);
				col++;
			} else {
				p++;
			}
		}
		printf("%d\n", row);
		row++;
	}
	delete_lp(lp);

	// int i, j;
	// for (int i=0; i<num_nodes; i++)
	// {
	//     for(int j=0; j<num_nodes; j++)
	//          printf("%d     ", adj_matrix[i*500 + j]);
	//     printf("\n");
	//  }

}
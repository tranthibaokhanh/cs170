'''
Sandbox for creating graphs and solving them. I would love to 
eventually be able to throw something heavy-handed at this like FTRL-proximal
or FNNs. 

I guess steps go:

1. Generate and visualize basic graphs.
2. Determine how to generate basic graphs with known cost.
	a. I actually think this is impossible for big hard instances.
3. Test a random field of graphs on Greedy Algs
	a. Select hard instances
4. Think really hard to figure out a type of min-cut (maybe?) algorithm
	a. Select hard instances
5. Something more powerful?
	a. Select hard instances

NOTES:

A. Input instances have 
		Line 1 specifies num_nodes.
		Line 2 specifies which nodes are children.
		Lines 3+ specify the adjacency matrix.
		No self-edges

B. nx.simple_cycles(G) will find all simple cycles.

C. Right now this is adult/child agnostic

D. General greedy format goes:
	a. Find all length 5 or less cycles.
	b. Select 1 based off a heuristic
	c. Delete all nodes in that cycle.
	d. Iterate

'''

from collections import deque

import copy
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time


MAX_NODES = 500

# Simple cycles from nx is good. Maybe too slow?
# Bottleneck is time to compute all cycles

def generate_basic_random(num_nodes=10, c=0.25):
	'''
	Generate a random graph with |V| = num_nodes and the chance for a
	directed edge from a --> b equal to c.
	'''

	if num_nodes > MAX_NODES:
		raise ValueError('Num_nodes is greater than the node cap.')

	if (c < 0) or (c> 1):
		raise ValueError('Connectivity must be between 0 and 1')

	l = np.clip(np.random.choice([0,1], size=(num_nodes,num_nodes), p=[1-c, c]) - np.eye(num_nodes), 0, 1)
	return l

def nx_vis(l, vis=False):
	'''
	Return graph of adjacency matrix using random node locations.
	Creates graph figure if vis=True
	'''

	# Number of nodes
	n = l.shape[0]

	# Random node location (to visualize)
	nodes = [[np.cos(2*np.pi*i / float(n)),np.sin(2*np.pi*i / float(n))]  for i in xrange(n)]

	G = nx.DiGraph(l)

	if vis:
		nx.draw(G, nodes)
		plt.show()
	return G

def G1_solver(l, children):
	'''
	Cycle Heuristic: Longer cycles are better
	'''
	#l, cost = trim(l)
	n = l.shape[0]
	cost = n

	G = nx.DiGraph(l)
	cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
	chosen_cycle = None
	
	while (cycles):
		max_cycle_value = 0
		for cycle in cycles:
			cycle_val = 0
			for node in cycle:
				if node in children:
					cycle_val += 2
				else:
					cycle_val += 1
			if cycle_val >= max_cycle_value:
				max_cycle_value = cycle_val
				chosen_cycle = cycle

		#print("G1 Chosen Cycle " + str(chosen_cycle))
		l = np.delete(l, chosen_cycle, axis=0)
		l = np.delete(l, chosen_cycle, axis=1)
		#print("Matrix " + str(l))

		G = nx.DiGraph(l)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
		cost -= max_cycle_value

	return cost

def G1_B_solver(l, children, num_trials = 3):
	'''
	Cycle Heuristic: Longer cycles are better
	'''
	curr_min = float("inf")

	l_permanent = np.copy(l)

	for _ in xrange(num_trials):
		l = l_permanent

		G = nx.DiGraph(l)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
		chosen_cycle = None

		while (cycles):

			max_cycle_value = 0
			cycle_costs = list()
			pos_idx = list()
			for cycle in cycles:
				cycle_val = 0
				for node in cycle:
					if node in children:
						cycle_val += 2
					else:
						cycle_val += 1
				cycle_costs.extend([cycle_val])
				if cycle_val >= max_cycle_value:
					max_cycle_value = cycle_val
					chosen_cycle = cycle
			for i in range(len(cycle_costs)):
				if cycle_costs[i] == max_cycle_value:
					pos_idx.extend([i])
			# pos_idx = [i for i,x in enumerate(cycles) if x == max(cycles, key=len)]
			# print pos_idx
			
			i = np.random.choice(pos_idx)

			l = np.delete(l, cycles[i], axis=0)
			l = np.delete(l, cycles[i], axis=1)

			G = nx.DiGraph(l)
			cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]

		curr_min = min(curr_min, l.shape[0])

	return curr_min

def G2_solver(l):
	'''
	Cycle Heuristic: Cycles with less popular nodes are better
	'''
	n = l.shape[0]

	# Create adjacency graph
	G = nx.DiGraph(l)
	cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
	chosen_cycle = None
	node_weights = np.zeros(n)

	# While we still have valid cycles to choose from
	while (cycles):

		#### Select cycle based off of inverse popularity

		# Update popularity matrix
		node_weights = np.zeros(n)
		for x in cycles:
			for i in x:
				node_weights[i] += 1.0

		chosen_cycle = max(cycles, key=lambda x: sum([1/node_weights[i] for i in x]))
		#print("G2 Chosen Cycle " + str(chosen_cycle))
		l = np.delete(l, chosen_cycle, axis=0)
		l = np.delete(l, chosen_cycle, axis=1)

		G = nx.DiGraph(l)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]

	return l.shape[0] 

def R1_solver(l, children, num_trials = 50):
	'''
	Cycle Heuristic: None. Random-walk over possible cycles.
	Hopefully we saturate the solution space.
	'''

	n = l.shape[0]
	curr_min = n + len(children)

	l_permanent = np.copy(l)
	best_cycles = []

	G = nx.DiGraph(l)
	cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]

	for _ in xrange(num_trials):
		new_min = n

		# Random ordering
		np.random.shuffle(cycles)

		I = set()
		chosen_cycles = []

		# Include cycles by order
		for c in cycles:

			if not (set(c) & I):
				chosen_cycles.append(c)
				I = I.union(c)

		new_min = n - len(I)
		for i in I:
			if i in children:
				new_min -= 1

		if new_min < curr_min:
			curr_min = new_min
			best_cycles = chosen_cycles

	return curr_min, best_cycles


def paper_solver(l):
	'''
	Input: Directed complete graph G = (V,E), |V|=n; edge weights w: E --> N 
	Ouput: an L-cycle Cover C^apx of G if n is L-admissible, _|_ otherwise.
	'''

	return # Not implemented

def P1_solver(l, num_iters=10, int_iters=25):
	'''
	Cycle Heuristic: Iterative. Right now it's bad? Or useless at best.

	- Perturbative model. Greedy search a solution. 
	- Pop a single cycle off the model. 
	- Pull Greedy over the new problem. If better than old solution, keep it.
	- Iterate.
	'''
	n = l.shape[0]
	curr_min_val = n
	curr_min_cycles = deque()

	l_permanent = np.copy(l)

	### Initially solve this problem with a random pull.
	G = nx.DiGraph(l)
	cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
	chosen_cycle = None
	
	while (cycles):
		c = max(cycles, key=len)

		# Zero out rows and cols (i.e. remove node)
		for i in c:
			l[c][:] = 0
			l[:][c] = 0
			curr_min_val -= 1
		
		curr_min_cycles.append(c)
		G = nx.DiGraph(l)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]


	#print("Initial P1 Error: " + str(curr_min_val))

	### Update call
	last_l = np.copy(l)
	last_min_cycles = copy.deepcopy(curr_min_cycles)
	last_min_val = curr_min_val

	for _ in xrange(num_iters):

		# Remove a single cycle.
		curr_min_cycles = copy.deepcopy(last_min_cycles)
		curr_l = np.copy(last_l)

		if not curr_min_cycles:
			c = []
			break
		else:
			c = curr_min_cycles.pop()

		# Re-insert rows and cols (i.e. add nodes back in)
		for i in c:
			for x in xrange(n):
				curr_l[x][i] = l_permanent[x][i]
				curr_l[i][x] = l_permanent[i][x]
			curr_min_val += 1

		G = nx.DiGraph(l)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
		chosen_cycle = None

		# Random walk over new graph.
		min_cost, best_cycles = R1_solver(curr_l, num_trials = int_iters)

		for c in best_cycles:
			for i in c:
				curr_l[c][:] = 0
				curr_l[:][c] = 0
			curr_min_cycles.append(c)

		if min_cost < last_min_val:
			last_min_val = min_cost
			last_l = np.copy(curr_l)
			last_min_cycles = copy.deepcopy(curr_min_cycles)


	return last_min_val

def GWMAX(l):
	'''
	Reduction to Maximum Weighted Independent Set
	The sets here are all simple cycles. Weights are cost of nodes.
	That is, number of nodes plus number of children.

	To speed up, break up initial graph. By SCCs maybe.

	GWMIN, GWMAX, and one more.
	http://www.sciencedirect.com/science/article/pii/S0166218X02002056
	'''
	N = l.shape[0]

	G = nx.DiGraph(l)

	# This may be incredibly slow.
	cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]

	n = len(cycles)

	# Make each cycle a node.
	V = xrange(len(cycles))

	# Give each node a weight.
	W = [len(x) for x in cycles]

	# Undirected graph g
	E = np.zeros((n, n))

	for i in xrange(n):
		for j in xrange(n):
			if bool(set(cycles[i]) & set(cycles[j])):
				E[i][j] = 1
				E[j][i] = 1

	# START GWMAX
	I = []
	i = 0

	while V:
		# Choose vertex v
		
		# This is good
		v_i = max(V, key= lambda i: W[i]/(sum(E[i][:]) + 1))

		# This is worse
		#v_i = min(V, key = lambda i: sum(E[i][:]))

		def new_order(i, W, E):
			conflict_nodes = [x for x in V if E[v_i][x]] + [v_i]

			w = W[i]
			d = sum(W[k] for k in conflict_nodes)
			return w/float(d)
		conflict_nodes = [x for x in V if E[v_i][x]] + [v_i]
		V = [x for x in V if x not in conflict_nodes]

		# Rule out these cycles.
		for c in conflict_nodes:
			E[c][:] = 0
			E[:][c] = 0

		I.append(v_i)
		i = i + 1

	chosen_cycles = [cycles[k] for k in I]
	#print("Chosen cycles: " + str(chosen_cycles))
	removed_cost = sum([len(x) for x in chosen_cycles])
	return N - removed_cost


def basic_compare(num_nodes=10, c=0.25, vis=False):
	# Generate adjacency matrix
	# l_perm = generate_basic_random(num_nodes, c)
	l_perm, children = create_greedy_destructor()
	# print "CHILDREN: "
	# print children
	G = nx.Graph()
	# print H
	l = np.copy(l_perm)
	costs = []
	times = []
	l_list = []

	# Visualize graph.
	G = nx_vis(l, vis=vis)

	# First Greedy Cost:
	start = time.clock()
	G1_cost = G1_solver(l, children)
	#print("G1 Cost: " + str(G1_cost))
	G1_time = time.clock() - start
	costs.append(G1_cost)
	times.append(G1_time)

	# First Greedy Cost Jittered:
	start = time.clock()
	G1_B_cost = G1_B_solver(l, children, num_trials=1)
	G1_B_time = time.clock() - start
	costs.append(G1_B_cost)
	times.append(G1_B_time)

	# Second Greedy Cost:
	start = time.clock()
	G2_cost = G2_solver(l)
	#print("G2 Cost: " + str(G2_cost))
	G2_time = time.clock() - start
	costs.append(G2_cost)
	times.append(G2_time)

	# Pure Random Cycle Selection Cost:
	start = time.clock()
	R1_cost, best_cycles = R1_solver(l, children, num_trials=1000)
	#print("G2 Cost: " + str(G2_cost))
	R1_time = time.clock() - start
	costs.append(R1_cost)
	times.append(R1_time)

	# Iterative Random Walk from Greedy
	start = time.clock()
	P1_cost = 0 #P1_solver(l, num_iters=5, int_iters=5)
	#print("G2 Cost: " + str(G2_cost))
	P1_time = time.clock() - start
	costs.append(P1_cost)
	times.append(P1_time)

	# Iterative Random Walk from Greedy
	start = time.clock()
	GWMAX_cost = GWMAX(l_perm)
	#print("G2 Cost: " + str(G2_cost))
	GWMAX_time = time.clock() - start
	costs.append(GWMAX_cost)
	times.append(GWMAX_time)

	#if R1_cost < G1_B_cost or G2_cost < G1_B_cost:
		#G = nx_vis(l_perm, vis=True)

	#if (R1_cost < G1_cost) and (R1_cost < G2_cost):
		#l_list = [l_perm]
	print(G1_cost - R1_cost), (G2_cost - R1_cost)
		#G = nx_vis(l_perm, vis=True)

	if ((G1_cost - R1_cost) >= 2) and ((G2_cost - R1_cost) >= 2):
		l_list = l_perm

	return np.array(costs), np.array(times), l_list

def basic_explore(num_nodes=10, c=0.25, num_iters=10, vis=False):

	scores = np.zeros(6)
	times = np.zeros(6)
	l_list = []
	if num_iters == 'fill':
		while len(l_list) < (500 / num_nodes):
			print("hi")

	else:
		for i in xrange(num_iters):
			(s, t, good_l) = basic_compare(num_nodes, c, vis)
			scores += s
			times += t
			# if good_l:
			# 	l_list += good_l

	scores = scores / num_iters
	times = times / num_iters

	print("G1 Cost: " + str(scores[0]) + " Time " + str(times[0]))
	print("G1_B Cost: " + str(scores[1]) + " Time " + str(times[1]))	
	print("G2 Cost: " + str(scores[2]) + " Time " + str(times[2]))
	print("R1 Cost: " + str(scores[3]) + " Time " + str(times[3]))
	#print("P1 Cost: " + str(scores[4]) + " Time " + str(times[4]))
	print("GWMAX Cost: " + str(scores[5]) + " Time " + str(times[5]))
	
	return combine_matrices(l_list)

def time_exploration(num_nodes = 10, c = 0.1):
	start = time.clock()
	l = generate_basic_random(num_nodes, c)
	G = nx_vis(l, vis=False)
	print(len([x for x in nx.simple_cycles(G) ]))
	print("Time: " + str(time.clock() - start))
	return

def create_greedy_destructor():
	G = nx.Graph()
	H = list(xrange(1, 66))
	# the adults are 6, 10, 14, 18, 22
	G.add_nodes_from(H)
	children = H.remove(6)
	children = H.remove(10)
	children = H.remove(14)
	children = H.remove(18)
	children = H.remove(22)

	G.add_edges_from([(1,2),(2,3),(3,4),(4,5),(5,1)])
	G.add_edges_from([(1,6),(6,7),(7,8),(8,9),(9,1)])
	G.add_edges_from([(2,10),(10,11),(11,12),(12,13),(13,2)])
	G.add_edges_from([(3,14),(14,15),(15,16),(16,17),(17,3)])
	G.add_edges_from([(4,18),(18,19),(19,20),(20,21),(21,4)])
	G.add_edges_from([(5,22),(22,23),(23,24),(24,25),(25,5)])

	G.add_edges_from([(6,26),(26,27),(27,28),(28,29),(29,6)])
	G.add_edges_from([(26,46),(27,47),(28,48),(29,49)])
	G.add_edges_from([(46,26),(47,27),(48,28),(49,29)])

	G.add_edges_from([(10,30),(30,31),(31,32),(32,33),(33,10)])
	G.add_edges_from([(30,50),(31,51),(32,52),(33,53)])
	G.add_edges_from([(50,30),(51,31),(52,32),(53,33)])

	G.add_edges_from([(14,34),(34,35),(35,36),(36,37),(37,14)])
	G.add_edges_from([(34,54),(35,55),(36,56),(37,57)])
	G.add_edges_from([(54,34),(55,35),(56,36),(57,37)])

	G.add_edges_from([(18,38),(38,39),(39,40),(40,41),(41,18)])
	G.add_edges_from([(38,58),(39,59),(40,60),(41,61)])
	G.add_edges_from([(58,38),(59,39),(60,40),(61,41)])

	G.add_edges_from([(22,42),(42,43),(43,44),(44,45),(45,22)])
	G.add_edges_from([(42,62),(43,63),(44,64),(45,65)])
	G.add_edges_from([(62,42),(63,43),(64,44),(65,45)])

	l = nx.to_numpy_matrix(G)
	print l.shape
	l, children = combine_matrices_with_children([l,l,l,l,l,l,l], H)
	# print children
	return (l, children)

def write_matrix(A, children, filename):
	# Open target file
	target = open(filename, 'w')

	# Write number of nodes
	num_nodes = A.shape[0]
	target.write(str(num_nodes))
	target.write("\n")

	# Write children
	for c in children:
		target.write(str(c) + " ")
	target.write("\n")

	# Write matrix
	for i in xrange(num_nodes):
		for x in xrange(num_nodes):
			target.write(str(int(A[i][x])) + " ")
		target.write("\n")

def combine_matrices_with_children(l_list, children):
	'''
	Create one big adjacency matrix out of lots of smaller ones.
	'''
	l_lens = [x.shape[0] for x in l_list]
	N = sum(l_lens)
	more_children = list()

	A = np.zeros((N, N))
	p = 0

	# Fill with independent submatrices
	for i in xrange(len(l_list)):
		l = l_list[i]
		for child in children:
			more_children.extend([child+p])
		for x in xrange(l_lens[i]):
			for y in xrange(l_lens[i]):
					A[x+p][y+p] = l.item(x,y)

		p += l_lens[i]

	return (A, more_children)

def combine_matrices(l_list, children=None):
	'''
	Create one big adjacency matrix out of lots of smaller ones.
	'''
	l_lens = [x.shape[0] for x in l_list]
	N = sum(l_lens)

	A = np.zeros((N, N))
	p = 0

	# Fill with independent submatrices
	for i in xrange(len(l_list)):
		l = l_list[i]
		for x in xrange(l_lens[i]):
			for y in xrange(l_lens[i]):
					A[x+p][y+p] = l.item(x,y)

		p += l_lens[i]

	return A

#### Actual function call ####

if __name__ == "__main__":
	# Call Basic Explore.
	# 	A. Generates a random adjacency matrix with connectivity c
	#	B. Calls four different solvers to solve this matrix
	#	C. Reports Cost and Runtime (averaged over 5 random matrices)
	#
	# 	Keep num_nodes low for exploration. 

	A = basic_explore(num_nodes=50, c=0.02, num_iters=100, vis=False)
	# l, children = create_greedy_destructor()
	# write_matrix(l, children, "10KIF3.in")
	#print(A)



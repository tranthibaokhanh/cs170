'''
Grabbing complete graph files and figuring out how many nodes they have to break them up into 2 or 3 cycles.

'''
from __future__ import division
from collections import deque

import copy
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time

def complete_graph_solver(instance_files, destination_file):

	complete_graphs = [9, 43, 77, 82, 152, 174, 199, 223, 231, 280, 298, 300, 326, 328, 365, 376, 385, 410, 416, 447, 452, 473, 488]
	index = 0
	destination = open(destination_file, 'w')
	for instance_file in instance_files:
		# Give each instance a header in the destination file
		first_line = "instance #: " + str(complete_graphs[index]) + '\n'
		destination.write(first_line)

		second_line = ""
		instance = open(instance_file, 'r')
		num_of_nodes = int(instance.readline())

		if num_of_nodes % 2 == 0:
			node = 1
			while node <= num_of_nodes:
				second_line += ' ' + str(node) + ' ' + str(node + 1)
				if node + 1 < num_of_nodes:
					second_line += ';'
				else:
					second_line += '\n'
				node += 2
			destination.write(second_line)
		else:
			node = 1
			while node <= num_of_nodes - 3:
				second_line += ' ' + str(node) + ' ' + str(node + 1) + ';'
				node += 2
			second_line += ' ' + str(node) + ' ' + str(node + 1) + ' ' + str(node + 2) + '\n' 
			destination.write(second_line)
			print complete_graphs[index]
		index += 1

			

if __name__ == "__main__":

	input_files = list()
	complete_graphs = ["9.in", "43.in", "77.in", "82.in", "152.in", "174.in", "199.in", "223.in", "231.in", "280.in", "298.in", "300.in", "326.in", "328.in", "365.in", "376.in", "385.in", "410.in", "416.in", "447.in", "452.in", "473.in", "488.in"]
	for graph in complete_graphs:
		input_file = "instances/" + graph
		input_files.extend([input_file])

	destination_file = 'complete_graphs.out'
	complete_graph_solver(input_files, destination_file)

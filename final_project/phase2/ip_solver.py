'''
An integer programming solver for random graphs. http://ac.els-cdn.com/S0377221713004244/1-s2.0-S0377221713004244-main.pdf?_tid=231ec8fe-0e42-11e6-9e9f-00000aab0f6b&acdnat=1461958970_86b37d158faacbdb2586b97c5109a993
Two approaches are:
1) Edge formulation.
2) Extended edge formulation
'''
from __future__ import division
from collections import deque
from pulp import * 

import copy
import numpy as np
from numpy import linalg as LA
import networkx as nx
from matplotlib import pyplot, patches, pylab
import time
import multiprocessing


def lp_solver(instance_files, destination_file, input_file):
	index = 0
	# destination = open(destination_file, 'w')
	for instance_file in instance_files:
		# Give each instance a header in the destination file
		# first_line = "Instance number: " + str(index) + '\n'
		# destination.write(first_line)
		instance_i = input_file[index]
		print instance_i
		instance = open(instance_file, 'r')
		lines = instance.readlines()
		last_line = lines[-1]
		matrix_input = ''

		# 1. Number of nodes
		i = 1
		for line in lines:
			if line is lines[0]:
				num_of_nodes = line
				# destination.write("# of nodes: " + str(num_of_nodes))
				num_of_nodes = int(num_of_nodes)
			elif line is lines[1]:
				children = line
				children = map(int, children.split())

			# Constructing the matrix to work with here
			elif i <= int(num_of_nodes)+ 2:
				# print str(int(num_of_nodes)+ 2)
				matrix_input += line
				if i != int(num_of_nodes)+ 2:
					matrix_input += ';'
			else:
				print "false line detected"
			i += 1
		# print matrix_input
		print "Done constructing the matrix"
		matrix = np.matrix(matrix_input)
		G = nx.from_numpy_matrix(matrix, create_using = nx.DiGraph())
		num_of_edges = len(G.edges())
		# p = multiprocessing.Process(target=edge_formulation, name='EF', args=(matrix, G, num_of_nodes, children, instance_i))
		# p.start()
		# time.sleep(240)

		# if p.is_alive():
		# 	print instance_i
		# 	print 'calculation took too long'
		# 	p.terminate()
		# p.join()
		edge_formulation(matrix, G, num_of_nodes, children, instance_i)

		index += 1


def edge_formulation(matrix, G, nodes, children, instance):
	# Instantiate the LP problem
	problem_name = "instance_" + str(instance)
	instance = LpProblem("", LpMaximize)

	num_of_edges = len(G.edges())
	cat = 'Integer'

	# 1) Construct the variable dictionary
	# AND
	# 2) Building the objective function by figuring out the weights of every arc from the matrix
	variable_dict = dict()
	weights_dict = dict()
	objective_function = 0
	for i in range(1, 1 + nodes):
		outgoing_sum_i = 0
		incoming_sum_i = 0
		print i
		for j in range(1, 1 + nodes):
			padding_i = ""
			padding_j = ""
			if i < 10:
				padding_i = "00"
			if i >= 10 and i < 100:
				padding_i = "0"
			if j < 10:
				padding_j = "00"
			if j >= 10 and j < 100:
				padding_j = "0"

			# Make sure this edge exists
			if matrix.item((i-1,j-1)) == 1:

				variable_name = "x_" + padding_i + str(i) + "_" + padding_j +str(j)
				# print variable_name

				if variable_name not in variable_dict.keys():
					x_ij = LpVariable(variable_name, lowBound=0, upBound=1, cat=cat)
					variable_dict[variable_name] = x_ij
					instance += variable_dict[variable_name] <= 1, "Singular Constraint: " + variable_name

				outgoing_sum_i += variable_dict[variable_name]

				if j in children:
					weights_dict[variable_name] = 2
				else:
					weights_dict[variable_name] = 1

				objective_function += weights_dict[variable_name]*variable_dict[variable_name]

			if matrix.item((j-1, i-1)) == 1:

				variable_name = "x_" + padding_j + str(j) + "_" + padding_i + str(i)
				# print variable_name

				if variable_name not in variable_dict.keys():
					# x_ji = LpVariable(variable_name, lowBound=0, upBound=1, cat='Continuous')
					x_ji = LpVariable(variable_name, lowBound=0, upBound=1, cat=cat)
					variable_dict[variable_name] = x_ji
					instance += variable_dict[variable_name] <= 1, "Singular Constraint: " + variable_name

				incoming_sum_i += variable_dict[variable_name]

		# constraint (1b)
		instance += outgoing_sum_i == incoming_sum_i, "Outcoming == Incoming: " + str(i)
		instance += outgoing_sum_i <= 1, "Outcoming <= 1: " + str(i)
		instance += incoming_sum_i <= 1, "Incoming <= 1: " + str(i)

	instance += objective_function
	print "Done Building the Objective Function"


	# find all paths of length 5
	paths = list()
	for i in range(nodes):
		path = findPaths(G, i, 6)
		paths.extend(path)
	print "Done Building paths"

	if len(paths) > 0:
		for path in paths:
			index = 0
			d_1 = 0
			while index < len(path) - 1:
				i = path[index] + 1
				j = path[index + 1] + 1
				padding_i = ""
				padding_j = ""
				if i < 10:
					padding_i = "00"
				if i >= 10 and i < 100:
					padding_i = "0"
				if j < 10:
					padding_j = "00"
				if j >= 10 and j < 100:
					padding_j = "0"
				variable_name = "x_" + padding_i + str(i) + "_" + padding_j +str(j)
				d_1 += variable_dict[variable_name]
				index += 1

			# print path
			# print d_1
			path = map(str,path)
			path_string = ''
			for p in path:
				path_string += p
			print path
			# constraint (1d)
			instance += d_1 <= 4, "For path: " + path_string
	print "Done Setting Path Constraints"
	print "going into solving the LP"
	instance.solve()
	instance.writeLP( "ip/"+ problem_name + ".lp")

	print("Status:", LpStatus[instance.status])

	destination = open('new_solutions/' + 'new/' + problem_name + '.out', 'w')
	for v in instance.variables(): 
		destination.write(v.name + "=" + str(v.varValue) + "\n")
	destination.write("Objective: " + str(value(instance.objective)) + "\n")
	total_cost = nodes + len(children)
	print total_cost
	destination.write("total_cost: " + str(total_cost) + "\n")
	solution_cost = total_cost - value(instance.objective)
	print solution_cost
	destination.write("solution_cost: " + str(solution_cost))

	print("Objective: ", value(instance.objective))
	
		
def findPaths(G,u,n):
    if n==0:
        return [[u]]
    paths = [[u]+path for neighbor in G.neighbors(u) for path in findPaths(G,neighbor,n-1) if u not in path]
    return paths

small_graphs = [2, 7, 17, 22, 26, 47, 60, 61, 63, 66, 70, 81, 84, 91, 95, 109, 139, 143, 144, 146, 149, 152, 153, 164, 185, 189, 196, 203, 204, 208, 209, 211, 216, 232, 240, 243, 247, 252, 260, 263, 272, 277, 285, 294, 305, 307, 309, 310, 311, 322, 335, 337, 339, 348, 371, 383, 387, 389, 401, 410, 417, 423, 425, 427, 435, 439, 464, 471, 478, 481, 485, 486]

to_solve = [15, 16, 29, 102]
definitely_solved = [1, 3, 4, 5, 6, 9, 10, 12, 19, 20, 21, 23, 25, 27, 28, 30, 31, 32, 33, 36, 39, 41, 42, 43, 44, 50, 52, 53, 55, 56, 57, 58, 62, 64, 66, 67, 68, 72, 75, 76, 77, 78, 79, 80, 82, 83, 86, 88, 89, 96, 97, 98, 100, 101, 103, 108, 112, 115, 116, 118, 119, 121, 125, 128, 129, 130, 133, 134, 135, 136, 138, 141, 142, 145, 146, 150, 152, 154, 157, 158, 159, 162, 163, 165, 166, 167, 173, 174, 175, 177, 179, 181, 182, 183, 186, 188, 190, 191, 192, 194, 195, 199, 202, 210, 212, 215, 219, 220, 223, 226, 229, 231, 236, 237, 239, 241, 247, 249, 250, 251, 253, 254, 255, 258, 260, 263, 264, 265, 266, 267, 268, 269, 271, 273, 274, 275, 276, 277, 280, 282, 284, 290, 291, 292, 295, 296, 298, 299, 300, 303, 313, 315, 316, 320, 323, 324, 326, 327, 328, 332, 333, 336, 337, 338, 341, 342, 343, 347, 350, 354, 356, 359, 363, 365, 366, 372, 374, 375, 376, 379, 380, 383, 384, 385, 388, 390, 393, 394, 395, 396, 397, 398, 402, 407, 408, 410, 412, 413, 415, 416, 418, 421, 424, 425, 426, 428, 429, 430, 431, 432, 437, 441, 442, 443, 446, 447, 450, 452, 455, 457, 458, 460, 461, 463, 468, 470, 471, 473, 475, 477, 478, 480, 484, 486, 488, 492]
definitely_solved += small_graphs

# to_check = map(int, ['104', '106', '110', '111', '114', '117', '120', '122', '137', '147', '148', '18', '243', '307', '335', '35', '38', '46', '481', '54', '63', '74', '8', '87', '90'])
to_check = [104]
if __name__ == "__main__":
	input_files = list()
	input_file_i = list()
	for i in to_check:
		if i not in to_solve:
			input_file = "instances/" + str(i) + ".in"
			input_files.extend([input_file])
			input_file_i.extend([i])
	destination_file = 'lp_solver_test.out'
	lp_solver(input_files, destination_file, input_file_i)


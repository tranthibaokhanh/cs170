'''
Write out formatted answers
'''
from __future__ import division
from collections import deque
from pulp import * 

import copy
import numpy as np
from numpy import linalg as LA
import networkx as nx
from matplotlib import pyplot, patches, pylab
import time
import multiprocessing
import glob, os


def output_format(instance_files, destination_file, input_file):
	index = 0
	destination = open(destination_file, 'w')
	instances_to_check = list()
	for instance_file in instance_files:
		# Give each instance a header in the destination file
		instance_i = input_file[index]
		first_line = str(instance_i) + ' cycles: '
		destination.write(first_line)
		# print instance_i
		instance = open(instance_file, 'r')
		lines = instance.readlines()
		last_line = lines[-1]
		G = nx.DiGraph()
		nodes = list()
		# 1. Number of nodes
		# print lines[0]
		if lines[0][0] != "x":
			# print "HERE"
			index += 1
			continue
		else:
			for line in lines:
				if line != lines[-1] and line != lines[-2] and line != lines[-3]:
					line = line[2:]
					line =line.replace('=','_')
					line = line.replace('-','')
					line = line.replace('\n','')
					line = line.split("_")
					i = int(line[0])
					j = int(line[1])
					val = float(line[2])
					if i not in G.nodes():
						G.add_node(i)
					if j not in G.nodes():
						G.add_node(i)
					if val == 1:
						G.add_edge(i, j)
			cycles = list(nx.simple_cycles(G))
			for cycle in cycles:
				cycle = list(cycle)
				if len(cycle) > 5:
					print cycle
					instances_to_check.extend([instance_i])
					break
				elif len(cycle) > 1:
					line = ''
					for node in cycle:
						line += str(node)
						if node != cycle[-1]:
							line += ' '
						else: 
							line += '; '
					destination.write(line)
			index += 1
		destination.write("\n")
		destination.write("\n")
	print instances_to_check

if __name__ == "__main__":
	input_files = list()
	input_file_i = list()
	for file in os.listdir("new_solutions/new/"):
		if file != '.DS_Store' and file.endswith(".out"):
			input_files.extend(["new_solutions/" + file])
			instance = file[9:]
			input_file_i.extend([instance.replace('.out', '')])
	destination_file = 'output_format_test.out'
	output_format(input_files, destination_file, input_file_i)


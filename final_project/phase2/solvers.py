from collections import deque

import copy
import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import time
import ast

from mcl_clustering import *

import csv

from sandbox import processInput, R1_solver


MAX_NODES = 500

NUM_GRAPHS = 492

class fast_solver():
	def __init__(self, num_graphs=25, instances=None, solved=[]):

		# List of instances
		if instances:
			self.instances = instances
		else:
			self.instances = xrange(1, num_graphs+1)
		self.num_graphs = num_graphs

		# NUM_GRAPHS x 1
		self.adj_mats = None

		# All solutions. Updated as things happen.
		self.optimal_solutions = np.ones((492, 2), dtype=np.ndarray)*9999

		# Running solutions list for this iteration
		self.solutions = np.ones((492, 2), dtype=np.ndarray)*9999

		# List of solved instances
		self.solved = solved

	def load_graphs(self, sols=False):
		# Load all graphs into self.graphs
		graphs = np.zeros((self.num_graphs, 3), dtype=np.ndarray)

		for i in self.instances:
			N, children, d = processInput('phase1-processed/' + str(i) + '.in')

			# Zero index	
			graphs[i-1][0] = N
			graphs[i-1][1] = children
			graphs[i-1][2] = d

		self.adj_mats = graphs

		if sols:
			with open('solutions.csv', 'rb') as csvfile:
				csvreader = csv.reader(csvfile, delimiter=",")
				for i in xrange(492):
					row = csvreader.next()
					self.optimal_solutions[i][0] = int(row[0])
					self.optimal_solutions[i][1] = row[1]

		return "Successful"

	def write_solutions(self):
		# Note the zero solutions. Might be useful later.
		zeros = []

		with open('solutions.csv', 'wb') as csvfile:
			csvwriter = csv.writer(csvfile, delimiter=",")
			for i in xrange(492):
				if self.optimal_solutions[i][0] == 0:
					zeros.append(i+1)
				if self.solutions[i][0] < self.optimal_solutions[i][0]:
					print("Improvement for case " +str(i+1) +" !!!")
					csvwriter.writerow((self.solutions[i]))
				else:
					csvwriter.writerow((self.optimal_solutions[i]))
		#print(zeros)


	def S1_solve(self, N, children, d, rarity=False, child_aware=False, prev=True):
		'''
		Input:
			N: Number of nodes in instance
			children: array of children nodes in graphs
			d: NxN adjacency matrix for this instance 

		Output:
			cost: Minimal cost found for this instance
			cycles: Cycles involved in this instance

		Heuristic: 
			For each node, grab the first cycle seen.
			Only consider length 2 cycles.

			TODO: Length 3+ cycles

		'''
		if rarity:
			d2 = np.dot(d,d)
		else:
			d2 = "erm"

		cost = N + len(children)
		cycles = []

		# List of already included nodes
		included_nodes = []

		nodes = list(xrange(N))

		# This shouldn't matter but hey.
		np.random.shuffle(nodes)

		# For each node
		for i in nodes:
			flag = 0

			# For each of its neighbors:
			neighbors = [x for x in xrange(N) if d[i][x]]
			np.random.shuffle(neighbors)
			if child_aware:
				neighbors.sort(key= lambda x: int(x in children), reverse=True)
			if rarity:
				neighbors.sort(key= lambda x: d2[x][x], reverse=False)


			for j in neighbors:

				# If there is a 2-cycle, zero out both.
				if prev & d[j][i]:

					# Add to cycles list
					cycles.append( (i,j) )
					cycles.append( (j,i) )

					d[i][:] = 0
					d[:][i] = 0

					d[j][:] = 0
					d[:][j] = 0

					cost -= 2

					if i in children:
						cost -= 1

					if j in children:
						cost -= 1

					break

				# neighbors_2 = [x for x in xrange(N) if d[j][x]]
				# np.random.shuffle(neighbors_2)
				# if child_aware:
				# 	neighbors.sort(key= lambda x: int(x in children), reverse=True)
				# if rarity:
				# 	neighbors.sort(key= lambda x: d2[x][x], reverse=False)

				# for k in neighbors_2:

				# 	# If there is a 2-cycle, zero out both.
				# 	if d[k][i]:

				# 		# Add to cycles list
				# 		cycles.append( (i,j) )
				# 		cycles.append( (j,k) )
				# 		cycles.append( (k,i) )

				# 		d[i][:] = 0
				# 		d[:][i] = 0

				# 		d[j][:] = 0
				# 		d[:][j] = 0

				# 		d[k][:] = 0
				# 		d[:][k] = 0

				# 		cost -= 3

				# 		if i in children:
				# 			cost -= 1

				# 		if j in children:
				# 			cost -= 1

				# 		if k in children:
				# 			cost -= 1

				# 		flag = 1
				# 		break
				# # Break to outside
				# if flag:
				# 	break

				# If there is a 2-cycle, zero out both.
				if (not prev) & d[j][i]:

					# Add to cycles list
					cycles.append( (i,j) )
					cycles.append( (j,i) )

					d[i][:] = 0
					d[:][i] = 0

					d[j][:] = 0
					d[:][j] = 0

					cost -= 2

					if i in children:
						cost -= 1

					if j in children:
						cost -= 1

					break

		return (cost, cycles)


	def Long_solve(self, N, children, d):
		'''
		Input:
			N: Number of nodes in instance
			children: array of children nodes in graphs
			d: NxN adjacency matrix for this instance 

		Output:
			cost: Minimal cost found for this instance
			cycles: Cycles involved in this instance

		Heuristic: 
			Probe down first. Return first cycle.
			

		'''
		cost = N + len(children)
		cycles = []

		# List of already included nodes
		included_nodes = []

		# For each node
		for i in xrange(N):
			flag = 0

			# For each of its neighbors:
			neighbors_1 = [x for x in xrange(N) if d[i][x]]
			np.random.shuffle(neighbors_1)

			for j in neighbors_1:

				# If there is a 2-cycle, zero out both.
				if d[j][i]:

					# Add to cycles list
					cycles.append( (i,j) )
					cycles.append( (j,i) )

					d[i][:] = 0
					d[:][i] = 0

					d[j][:] = 0
					d[:][j] = 0

					cost -= 2

					if i in children:
						cost -= 1

					if j in children:
						cost -= 1

					flag = 1
					break

				neighbors_2 = [x for x in xrange(N) if d[j][x]]
				np.random.shuffle(neighbors_2)

				for k in neighbors_2:

					# If there is a 2-cycle, zero out both.
					if d[k][i]:

						# Add to cycles list
						cycles.append( (i,j) )
						cycles.append( (j,k) )
						cycles.append( (k,i) )

						d[i][:] = 0
						d[:][i] = 0

						d[j][:] = 0
						d[:][j] = 0

						d[k][:] = 0
						d[:][k] = 0

						cost -= 3

						if i in children:
							cost -= 1

						if j in children:
							cost -= 1

						if k in children:
							cost -= 1

						flag = 1
						break

					neighbors_3 = [x for x in xrange(N) if d[k][x]]
					np.random.shuffle(neighbors_3)

					for l in neighbors_3:

						# If there is a 2-cycle, zero out both.
						if d[l][i]:

							# Add to cycles list
							cycles.append( (i,j) )
							cycles.append( (j,k) )
							cycles.append( (k,l) )
							cycles.append( (l,i) )

							d[i][:] = 0
							d[:][i] = 0

							d[j][:] = 0
							d[:][j] = 0

							d[k][:] = 0
							d[:][k] = 0

							d[l][:] = 0
							d[:][l] = 0

							cost -= 4

							if i in children:
								cost -= 1

							if j in children:
								cost -= 1

							if k in children:
								cost -= 1

							if l in children:
								cost -= 1

							flag = 1
							break

					# Break to outermost loop
					if flag:
						break

				# Break to outermost loop
				if flag:
					break


		return (cost, cycles)

	def R1_solve(self, N, children, d, num_trials = 50):
		'''
		Cycle Heuristic: None. Random-walk over possible cycles.
		Hopefully we saturate the solution space.
		'''

		curr_min_cost = N + len(children)

		d_permanent = np.copy(d)
		best_cycles = []

		G = nx.DiGraph(d)
		cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]

		for _ in xrange(num_trials):
			new_min = N + len(children)

			# Random ordering
			np.random.shuffle(cycles)

			I = set()
			chosen_cycles = []

			# Include cycles by order
			for c in cycles:

				if not (set(c) & I):

					for k in xrange(len(c)):

						if k == 0:
							edge = (c[-1], c[0])
						else:
							edge = (c[k-1], c[k])
						chosen_cycles.append(edge)

						new_min -= 1
						if c[k] in children:
							new_min -= 1


						I = I.union(edge)


			if new_min < curr_min_cost:
				curr_min_cost = new_min
				best_cycles = chosen_cycles

		return curr_min_cost, best_cycles



	def explore(self):

		#visual_smalls = [2, 7, 17, 22, 26, 44, 47, 60, 61, 63, 66, 70, 81, 84, 91, 95, 109, 139, 143, 144, 146, 149, 152, 153, 164, 185, 189, 196, 203, 204, 208, 209, 211, 216, 232, 240, 243, 247, 252, 260, 263, 272, 277, 285, 294, 305, 307, 309, 310, 311, 322, 335, 337, 339, 348, 371, 383, 387, 389, 401, 410, 417, 423, 425, 427, 435, 439, 464, 471, 478, 481, 485, 486]

		# ip_1700_smalls = [2, 6, 7, 8, 11, 13, 14, 17, 18, 22, 24, 26, 32, 34, 35, 37, 40, 44, 45, 47, 
		# 			48, 49, 51, 59, 60, 61, 63, 65, 66, 69, 70, 71, 73, 76, 80, 81, 84, 85, 87, 90,
		# 			 91, 92, 93, 94, 95, 97, 99, 104, 105, 107, 108, 109, 110, 111, 113, 120, 123, 
		# 			 124, 126, 127, 131, 132, 137, 139, 140, 143, 144, 146, 147, 148, 149, 151, 152, 
		# 			 153, 155, 160, 161, 164, 169, 171, 172, 176, 178, 184, 185, 187, 189, 193, 194, 
		# 			 196, 200, 201, 203, 204, 206, 207, 208, 209, 211, 213, 214, 216, 217, 218, 222, 
		# 			 224, 225, 227, 228, 230, 231, 232, 233, 235, 240, 243, 245, 246, 247, 248, 252, 
		# 			 255, 257, 259, 260, 261, 262, 263, 272, 277, 278, 281, 283, 285, 286, 287, 289, 
		# 			 293, 294, 297, 300, 301, 302, 304, 305, 306, 307, 309, 310, 311, 312, 314, 317, 
		# 			 318, 322, 326, 329, 330, 331, 334, 335, 337, 339, 340, 345, 346, 348, 349, 355, 
		# 			 358, 361, 362, 364, 365, 367, 368, 371, 373, 377, 382, 383, 386, 387, 389, 392, 
		# 			 399, 401, 405, 406, 410, 411, 417, 419, 420, 423, 425, 427, 434, 435, 436, 438, 
		# 			 439, 444, 445, 448, 449, 453, 454, 456, 460, 464, 466, 467, 468, 469, 471, 474, 
		# 			 476, 478, 479, 481, 482, 483, 485, 486, 487, 489, 490, 491]




		edges = 0
		elapsed = 0
		num = 0

		fulls = [9, 43, 77, 82, 146, 152, 174, 199, 223, 231, 277, 280, 298, 
				300, 326, 328, 365, 376, 385, 410, 416, 447, 452, 473, 486, 488]

		upper_triangulars = [50, 66, 108, 128, 146, 154, 258, 260, 263, 277, 315, 383, 418, 429, 486]

		block_diagonals = [11, 14, 34, 48, 49, 59, 65, 71, 85, 92, 105, 113, 127,
							147, 151, 155, 172, 214, 227, 246, 255, 257, 262, 278, 302,
							304, 352, 362, 364, 367, 391, 392, 399, 433, 434, 436, 444, 
							446, 448, 449, 454, 456, 466, 467, 487, 489, 491]

		less_50_edges = [7, 17, 22, 26, 40, 44, 47, 51, 60, 61, 66, 69, 70, 81, 84, 91, 
						95, 108, 109, 113, 124, 139, 143, 144, 146, 149, 152, 153, 164, 185, 
						189, 193, 196, 201, 203, 204, 208, 209, 211, 216, 232, 240, 243, 247, 
						252, 259, 260, 261, 263, 272, 277, 281, 285, 294, 306, 307, 309, 310, 
						311, 322, 326, 329, 335, 337, 339, 340, 346, 348, 365, 371, 383, 387, 
						389, 401, 410, 417, 423, 425, 427, 435, 439, 464, 471, 478, 481, 485, 
						486]



		# Diagonal with edges everywhere.
		mine = 352

		#for i in [x for x in smalls if x in self.instances]:
		for i in [x for x in block_diagonals if x in self.instances]:
			num += 1
			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]

			start = time.clock()
			num_edges = sum(sum(d))

			G = nx.DiGraph(d)
			print("Cycle: " +str(i))
			print(nx.number_strongly_connected_components(G))
			#print([x for x in nx.strongly_connected_components(G)])
			cycles = [x for x in nx.simple_cycles(G) if len(x) <= 5]
			# Various checks.

			# if num_edges <= 50:
			# 	few_edges.append(i)
			# if np.array_equal(d + np.eye(N), np.ones((N,N))):
			# 	fulls.append(i) 
			# if np.array_equal(d - np.triu(d), np.zeros((N,N))):
			# 	upper_triangulars.append(i)


			elapsed += time.clock() - start
		
		
		#print("Few edges: " + str(few_edges))
		#print("Full graphs: " + str(fulls))
		#print("Upper triangulars " + str(upper_triangulars))
		print( "Time to calc: " + str(elapsed) + " for " + str(num) + " Graphs.")

	def edge_case_solve(self):
		'''
		Solve edge instances specified below

		Updates solutions

		sol[i][0] = Instance Cost
		sol[i][1] = Instance Solution

		Notes: 
			This is all 1 indexed
			Zero Matrices: Are a subset of Upper Triangulars
			Check out blockmodel? ANd k-components.

		'''

		solved = []

		# LISTS OF GRAAAAAAPHS

		# Upper Triangular Graphs
		upper_triangulars = [50, 66, 108, 128, 146, 154, 258, 260, 263, 277, 315, 383,
							 418, 429, 486]
		solved += upper_triangulars

		# Full Graphs
		fulls = [9, 43, 77, 82, 146, 152, 174, 199, 223, 231, 277, 280, 298, 
				300, 326, 328, 365, 376, 385, 410, 416, 447, 452, 473, 486, 488]

		solved += fulls

		# Graphs where S1 got zero cost solutions. Ayy some of these are good
		S1_or_Long_solved = [3, 5, 6, 9, 10, 12, 20, 23, 25, 27, 30, 31, 32, 43, 44, 52, 53, 55, 64, 72, 77, 
					78, 80, 82, 89, 96, 97, 98, 100, 101, 115, 116, 119, 121, 133, 135, 142, 145, 152, 174, 179, 186,
					 188, 199, 210, 223, 231, 247, 255, 265, 268, 275, 280, 292, 298, 300, 313, 
					 326, 332, 337, 341, 385, 388, 394, 397, 407, 410, 415, 425, 441, 442, 447, 450, 452, 
					 457, 458, 471, 473, 475, 478, 484, 488]

		solved += S1_or_Long_solved

		# IP_solved = [2, 7, 11, 13, 14, 34, 48, 49, 85, 92, 93, 105, 107, 114, 127, 131, 140, 151, 
		# 			155, 172, 176, 187, 207, 214, 227, 233, 242, 245, 246, 262, 278, 287, 297, 
		# 			331, 334, 345, 360, 362, 364, 367, 368, 373, 382, 392, 399, 411, 434, 449,
		# 			 456, 466, 476, 479, 487, 489]

		# solved += IP_solved

		# visual_smalls = [2, 7, 17, 22, 26, 44, 47, 51, 60, 61, 63, 66, 70, 81, 84, 91, 95, 109, 139, 143, 
		# 				144, 146, 149, 152, 153, 164, 185, 189, 196, 203, 204, 208, 209, 211, 216, 232, 
		# 				240, 243, 247, 252, 260, 263, 272, 277, 285, 294, 305, 307, 309, 310, 311, 322, 
		# 				335, 337, 339, 348, 371, 383, 387, 389, 401, 410, 417, 423, 425, 427, 435, 439, 
		# 				464, 471, 478, 481, 485, 486]

		# solved += visual_smalls

		improve_sols_benefits = [1, 19, 21, 33, 39, 42, 57, 58, 62, 67,
								68, 75, 76, 79, 83, 86, 112, 125, 134,
								136, 138, 141, 150, 159, 162, 163, 166, 167,
								173, 175, 177, 181, 183, 190, 191, 192, 195,
								202, 215, 219, 220, 226, 229, 236, 237, 239,
								241, 250, 253, 254, 267, 269, 274, 276, 282,
								290, 291, 295, 296, 299, 303, 320, 323, 324, 327, 333,
								336, 338, 342, 343, 350, 354, 363, 374, 375, 379, 384,
								390, 396, 316, 372, 398, 402, 412, 413, 443, 461, 463, 
								470, 477, 480, 492]

		solved += improve_sols_benefits

		improve_sols_benefits2 = [28, 36, 41, 130, 194, 212, 251, 264, 266, 284, 347, 356, 366, 393, 408, 431, 432, 437, 460,
									421, 426, 428, 468]

		solved += improve_sols_benefits2

		improve_sols_benefits3 = [56, 88, 118, 157, 158, 165, 182, 249, 271, 273, 359, 395, 157, 430, 446, 
								129, 380, 455, 424, 103]

		solved += improve_sols_benefits3

		solved = list(set(solved))
		print(solved)
		to_solve = list(set(list(xrange(1,493))) - set(solved))
		print(to_solve)


		# Solve upper triangulars. Lol
		for i in [x for x in upper_triangulars if x in self.instances]:
			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]

			self.solutions[i-1][0] = N + len(children)
			self.solutions[i-1][1] = []

		# Solve fulls
		for i in [x for x in fulls if x in self.instances]:
			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]

			cycles = []

			if N == 1:
				cost = 0
				cycles = []

			elif (N % 2) == 1:

				cycles.append( (0,1) )
				cycles.append( (1,2) )
				cycles.append( (2,0) )

				for k in xrange(3,N):
					if k % 2 == 1:
						cycles.append( (k, k+1) )
					else:
						cycles.append( (k-1, k) )
			else:
				for k in xrange(N):
					if k % 2 == 1:
						cycles.append( (k-1, k) )
					else:
						cycles.append( (k, k+1) )

			self.solutions[i-1][0] = 0
			self.solutions[i-1][1] = cycles


		for i in [x for x in self.instances if x not in solved]:
		#for i in self.instances:

			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]


			if False:
				G = nx.DiGraph(d)
				nx.draw(G, pos=nx.spring_layout(G))
				plt.show()

			if sum(sum(np.triu(d))) >  sum(sum(np.tril(d))):
				d = np.transpose(d)

			# Solve instance
			cost1, cycles1 = self.S1_solve(N, children, d)
			cost2, cycles2 = 9999, [] #self.S1_solve(N, children, d, rarity=True)
			cost3, cycles3 = self.S1_solve(N, children, d, child_aware=True)
			cost4, cycles4 = self.S1_solve(N, children, d, prev=False)
			
			costs = [cost1, cost2, cost3, cost4]

			k = np.argmin(costs)
			cost = costs[k]
			cycles = [cycles1, cycles2, cycles3, cycles4][k]

			# Update solutions
			self.solutions[i-1][0] = cost
			self.solutions[i-1][1] = cycles

			print(str(i) + " " + str(self.optimal_solutions[i-1][0]))

			if cost < self.optimal_solutions[i-1][0]:
				print("IMPROVED COST FOR INSTANCE " + str(i) + " cost " + str(cost))
				self.optimal_solutions[i-1][0] = cost
				self.optimal_solutions[i-1][1] = cycles

		return "Successful"


	def solve(self, partition=False, solver='S1'):
		'''
		Solve all instances in self.graphs

		Returns sol, an NUM_GRAPHS x 2 array

		sol[i][0] = Instance Cost
		sol[i][1] = Instance Solution

		'''

		# NUM_GRAPHS x 2. Cost, Solution for each instance

		to_solve = [x for x in self.instances if x not in self.solved]

		for i in to_solve:

			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]

			if partition:
				d = self.partition(d)

			if sum(sum(np.triu(d))) >  sum(sum(np.tril(d))):
				d = np.transpose(d)

			# Solve instance
			if solver == 'S1':
				cost, cycles = self.S1_solve(N, children, d)
			elif solver == 'long':
				cost, cycles = self.Long_solve(N, children, d)
			elif solver =='adaptive':
				if sum(sum(d)) < 50:
					cost, cycles = self.Long_solve(N, children, d)
				else:
					cost, cycles = self.S1_solve(N, children, d)
			else:
				print "Error: Invalid solver"
				return


			# Update solutions
			self.solutions[i-1][0] = cost
			self.solutions[i-1][1] = cycles

			if cost < self.optimal_solutions[i-1][0]:
				self.optimal_solutions[i-1][0] = cost
				self.optimal_solutions[i-1][1] = cycles

		return "Successful"


	def improve_sols(self, i, num_iters=100, num_pop=10, num_trials=50, trim=False, fast_fail=True):
		'''

		Take in current best solutions and update.

		get current solution for graph i (zero indexed) by calling
		self.optimal_solutions[i-1].

		self.optimal_solution[i-1][0] is the cost
		self.optimal_solutions[i-1][1] is a list of edges in the cycle set.

		Input: Instance i (1 indexed)

		Update optimal solution.
		Breaks first time it sees a better solution, for now.

		Output: "Successful"
		'''
		N = self.adj_mats[i-1][0]
		children = self.adj_mats[i-1][1]
		d = self.adj_mats[i-1][2]

		lones = 0
		for x in xrange(N):
			if (sum(d[:][x]) == 0) or (sum(d[x][:]) == 0):
				lones += 1
				if x in children:
					lones += 1


		orig_cost = self.optimal_solutions[i-1][0]
		if orig_cost == 0:
			print(str(i) + " original cost " + str(orig_cost))
		if lones == orig_cost:
			print(str(i) + " is totally also solved")

		#print(str(i) + " difference " + str(orig_cost - lones))

		#return

		# Change this.
		if orig_cost == 0:
			print ("Instance " + str(i) + " has 0 cost")

		# if (orig_cost > 75):
		# 	return


		if trim or (i == 238) or (i == 321):
			s = np.zeros((N,N))
			for n in xrange(N):
				for m in xrange(n-48, n+49):
					if (m < 0) or (m > N-1):
						continue
					else:
						s[n][m] = d[n][m]
			d = s
			# plt.imshow(d, cmap='gray')
			# plt.show()

		orig_edges = ast.literal_eval(self.optimal_solutions[i-1][1])

		for _ in xrange(num_iters):
			curr_cost = N + len(children)

			# First remove some cycles.
			new_d = np.copy(d)


			# Nodes to pop off of cycles
			re_add_nodes = set(np.random.choice(xrange(N), size=num_pop))


			new_edges = [edge for edge in orig_edges if (not (set(edge) & re_add_nodes))]
			
			delete_nodes = set()
			for edge in new_edges:
				delete_nodes = delete_nodes | set(edge)

			for x in list(delete_nodes):
				new_d[x][:] = 0
				new_d[:][x] = 0

				curr_cost -= 1

				if x in children:
					curr_cost -= 1

			new_children = list(set(children) - set(delete_nodes))
			new_N = N - len(delete_nodes)

			new_cost, new_cycles = self.R1_solve(N=new_N, children=new_children, d=new_d, num_trials=num_trials)

			#print(orig_cost, new_cost)

			if new_cost < orig_cost:
				print("IMPROVED COST FOR INSTANCE " + str(i) + " cost " + str(new_cost))
				self.optimal_solutions[i-1][0] = new_cost
				self.optimal_solutions[i-1][1] = new_cycles + new_edges
				orig_cost = new_cost
				orig_edges = new_cycles
				if fast_fail:
					break

		#print("Lones cost " + str(lones))


		return "Successful"

	def partition(self, d):
		'''

		HEY AC WRITE THIS FUNCTION

		Partition is called in solve right now, when solve is called
		with partition = True. Might be helpful.

		Right now it just cuts the graph in half.

		Partition the graph, hopefully removing as 
		few cycles in the process as possible, or hopefully
		doing something smart.

		Input:
			- d: Original adjacency matrix
		Output:
			- partitions: List of partition indices

		'''
		N = d.shape[0]

		for i in xrange(N):
			for j in xrange(N):
				if (i < N/2) and (j > N/2):
					d[i][j] = 0

				if (i > N/2) and (j < N/2):
					d[i][j] = 0

		return d

	def convert_solutions(self, instances=xrange(1,493)):
		destination = open('solutions_5.out', 'w')
				
		for i in instances:

			####### This probably doesn't apply to IP people #########

			orig_edges = ast.literal_eval(self.optimal_solutions[i-1][1])
			# Generate instance
			N = self.adj_mats[i-1][0]
			children = self.adj_mats[i-1][1]
			d = self.adj_mats[i-1][2]

			s = np.zeros((N,N))
			for e in orig_edges:
				s[e[0]][e[1]] = 1

			##########################################################

			G = nx.DiGraph(s)
			cycles = list(nx.simple_cycles(G))

			if not cycles:
				destination.write('None\n')
			else:
				line = ''
				for i in xrange(len(cycles)):
					c = cycles[i]
					for e in range(len(c)):
						if e != 0:
							line += ' '
						line += str(c[e])
					if i != len(cycles) - 1:
						line += '; '
				line += '\n'
				destination.write(line)

	def merge_solutions(self, filename1, filename2, filename_out):

		with open(filename1, 'r') as csvfile1:
			csvreader1 = csv.reader(csvfile1)

			with open(filename2, 'r') as csvfile2:
				csvreader2 = csv.reader(csvfile2)

				with open(filename_out, 'wb') as csvfile_out:
					csvwriter = csv.writer(csvfile_out, delimiter=" ")

					for i in xrange(1, 493):

						# Instances
						N = self.adj_mats[i-1][0]
						children = self.adj_mats[i-1][1]
						d = self.adj_mats[i-1][2]

						row1 = csvreader1.next()
						edges1 = row1[0].replace(';','').split()
						row1 = row1[0].split()

						row2 = csvreader2.next()
						edges2 = row2[0].replace(';','').split()
						row2 = row2[0].split()

						cost1 = N + len(children)
						for e in edges1:
							cost1 -= 1
							if str(e) in children:
								cost1 -= 1

						cost2 = N + len(children)
						for e in edges2:
							cost2 -= 1
							if str(e) in children:
								cost1 -= 1

						print(cost1, cost2)

						if cost1 < cost2:
							print("chose 1")
							csvwriter.writerow(row1)
						else:
							print("chose 2")
							csvwriter.writerow(row2)

def read_solution(self):
	'''
	Hopefully it doesn't get this desperate.
	'''

	pass

if __name__ == "__main__":

	'''
	Reads in instances and best solutions found so far.

	Runs solver.

	Updates best solution by taking min between found solutions and best found
	so far.
	'''

	# Start Clock
	start = time.clock()

	# Specify solved instances
	ignore = set([])

	# Initialize solver. num_graphs = 492 for full. Or specify which instances to use. xrange(1,493) is full.
	#instances = [x for x in xrange(1,10)]

	to_solve = [15, 16, 18, 29, 38, 46, 54, 
			65, 87, 90, 102, 104, 106, 110, 111, 
			113, 117, 120, 122, 123, 124, 126, 129, 132, 137, 147, 148, 
			156, 160, 161, 168, 169, 170, 171, 178, 180, 184, 
			193, 197, 198, 200, 201, 205, 206, 213, 217, 218, 221, 222, 
			224, 225, 228, 230, 234, 235, 238, 244, 248, 256, 257, 259, 
			261, 270, 279, 281, 283, 286, 288, 289, 293, 301, 302, 304, 
			306, 308, 312, 314, 317, 318, 319, 321, 325, 329, 330, 340, 344, 346, 
			349, 351, 352, 353, 355, 357, 358, 361, 369, 370, 377, 378, 
			380, 381, 386, 391, 400, 403, 404, 405, 406, 409, 414, 419, 420, 
			422, 433, 436, 438, 440, 444, 445, 448, 451, 453, 
			454, 459, 462, 465, 467, 469, 472, 474, 482, 483, 490, 491]

	less_75_cost = [29, 38, 65, 87, 90, 104, 106, 113, 117, 120, 123, 137, 148, 161, 168,
					178, 180, 184, 198, 200, 201, 217, 225, 228, 256, 270,
					279, 283, 314, 370, 404, 419, 436, 440, 445, 451, 453,
					472, 474, 18, 205, 224, 289, 340, 351, 378, 403, 405, 438]

	solved = [1, 3, 4, 5, 6, 9, 10, 12, 19, 20, 21, 23, 25, 27, 28, 30, 31, 32, 33, 36, 39, 41, 42, 43, 44, 50, 52, 53, 55, 56, 57, 58, 62, 64, 66, 67, 68, 72, 75, 76, 77, 78, 79, 80, 82, 83, 86, 88, 89, 96, 97, 98, 100, 101, 103, 108, 112, 115, 116, 118, 119, 121, 125, 128, 129, 130, 133, 134, 135, 136, 138, 141, 142, 145, 146, 150, 152, 154, 157, 158, 159, 162, 163, 165, 166, 167, 173, 174, 175, 177, 179, 181, 182, 183, 186, 188, 190, 191, 192, 194, 195, 199, 202, 210, 212, 215, 219, 220, 223, 226, 229, 231, 236, 237, 239, 241, 247, 249, 250, 251, 253, 254, 255, 258, 260, 263, 264, 265, 266, 267, 268, 269, 271, 273, 274, 275, 276, 277, 280, 282, 284, 290, 291, 292, 295, 296, 298, 299, 300, 303, 313, 315, 316, 320, 323, 324, 326, 327, 328, 332, 333, 336, 337, 338, 341, 342, 343, 347, 350, 354, 356, 359, 363, 365, 366, 372, 374, 375, 376, 379, 380, 383, 384, 385, 388, 390, 393, 394, 395, 396, 397, 398, 402, 407, 408, 410, 412, 413, 415, 416, 418, 421, 424, 425, 426, 428, 429, 430, 431, 432, 437, 441, 442, 443, 446, 447, 450, 452, 455, 457, 458, 460, 461, 463, 468, 470, 471, 473, 475, 477, 478, 480, 484, 486, 488, 492]

	new_zeros = [70, 95, 149, 185, 187, 307, 196, 208, 373, 439, 232, 242, 
				272, 294, 309, 305, 368, 481, 335, 423, 35, 348, 427, 
				287, 435, 451, 472, 90]

	lone_nodes = [47, 51, 71, 81, 114, 120, 123, 252, 285]

	to_fix = [106, 8, 319, 171, 433, 456, 422,
				360, 111, 321, 148, 414, 222, 54, 46, 74,
				325, 352, 353, 403,
				217, 411, 168, 180, 200, 357, 
				314, 279, 147, 482, 137, 18, 197, 370, 270, 
				378, 474, 391, 462, 469, 117, 381, 302, 453, 
				221, 110, 308, 400, 445, 38, 351, 330, 122, 63, 262, 
				440, 344, 244, 87, 104, 404, 198, 29, 207,
				491, 286, 243, 288, 419, 465, 386, 205, 213, 35]

	solved += new_zeros
	solved += lone_nodes

	#single_shot = [x for x in xrange(1, 493) if x not in solved]
	single_shot = [147]

	flash = fast_solver(num_graphs = 492) #, instances = single_shot, solved= ignore)

	# Load graphs 

	load_start = time.clock()
	flash.load_graphs(sols=True)
	print("Loading time: " + str(time.clock() - load_start) + "\n")

	# Explore graphs
	#flash.explore()

	# Solve Time: G1 is 100 seconds, 
	solve_start = time.clock()

	#flash.solve(partition=False, solver='S1')
	#flash.edge_case_solve()

	# for x in single_shot:
	# 	flash.improve_sols(x, num_iters = 10, num_pop = 2, num_trials=5000, fast_fail=True)#trim=0)
	flash.convert_solutions(xrange(1,493))
	print("Solve Time: " + str(time.clock() - solve_start) +"\n")

	# Write solutions. Takes min between found solution and optimal found so far for each instance.
	flash.write_solutions()

	flash.merge_solutions('solutions_5.out', 'solutions_AC.out', 'solutions_merge.out')

	# End Clock
	print("\nTotal Time: " + str(time.clock() - start))